from django.shortcuts import render
from . import forms
from django.http import HttpResponseRedirect
from .forms import NameForm

from time import gmtime, strftime
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialToken
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.models import SocialLogin
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from django.views import generic
from django.views.generic.list import ListView


def get_name(request):
    if request.method == 'POST':
        print(request)
        form = NameForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thanks/')
    else:
        form = NameForm()
    return render(request, 'name.html', {'form':form})
    

#DataFlair #Form #View Functions
def regform(request):
    form = forms.SignUp()
    if request.method == 'POST':
        form = forms.SignUp(request.POST)
        html = 'we have recieved this form again'
    else:
        html = 'welcome for first time'
    return render(request, 'signup.html', {'html': html, 'form': form})

def twitterform(request):
    form = forms.TwitterForm()
    if request.method == 'POST':
        form = forms.TwitterForm(request.POST)
        html = 'twice again'
    else:
        html = 'first time'
    return render(request, 'twitterform.html',{'html':html,'form':form})

def test(request):
    return render(request,'test.html')



def hello_forms(request):
    form = forms.HelloForm(request.GET or None)
    if form.is_valid():
        message = 'success'
    else:
        message = 'fail'
    d = {'form': form, 'message':message,}
    return render(request, 'forms.html',d)


from django.shortcuts import render, redirect
from .models import Book

from django.http import HttpResponse
#DataFlair
def index(request):
    shelf = Book.objects.all()
    return render(request, 'book/library.html', {'shelf': shelf})
def upload(request):
    upload = BookCreate()
    if request.method == 'POST':
        upload = BookCreate(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('index')
        else:
            return HttpResponse("""your form is wrong, reload on <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'book/upload_form.html', {'upload_form':upload})

def update_book(request, book_id):
    book_id = int(book_id)
    try:
        book_sel = Book.objects.get(id = book_id)
    except Book.DoesNotExist:
        return redirect('index')
    book_form = BookCreate(request.POST or None, instance = book_sel)
    if book_form.is_valid():
        book_form.save()
        return redirect('index')
    return render(request, 'book/upload_form.html', {'upload_form':book_form})

def delete_book(request, book_id):
    book_id = int(book_id)
    try:
        book_sel = Book.objects.get(id = book_id)
    except Book.DoesNotExist:
        return redirect('index')
    book_sel.delete()
    return redirect('index')


def search(request):
    r = request.GET['your_name']
    if request.GET.get('your_name'):
        message = r + ' ,your name is  %r' % request.GET['your_name']
        request = request.GET['your_name']
    else:
        message = 'nothing'
    return HttpResponse(message)

def hello_get_query(request):
    name = request.GET.get('your_name')
    d = {'your_name': request.GET.get('your_name')}
    return render(request, 'get_query.html',d,{'name':name})
