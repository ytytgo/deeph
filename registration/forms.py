from django import forms
from .models import *
#DataFlair #Form
class SignUp(forms.Form):
    first_name = forms.CharField(initial = 'First Name', )
    last_name = forms.CharField()
    email = forms.EmailField(help_text = 'write your email', )
    Address = forms.CharField(required = False, )
    Technology = forms.CharField(initial = 'Django', disabled = True, )
    age = forms.IntegerField()
    password = forms.CharField(widget = forms.PasswordInput)
    re_password = forms.CharField(help_text = 'renter your password', widget = forms.PasswordInput)

    def clean_password(self):
        password = self.cleaned_data['passeword']
        if len(password) < 4:
            raise forms.ValidationError("pass is short")
        return password


class TwitterForm(forms.Form):
    username = forms.CharField(initial = 'username',)

class NameForm(forms.Form):
    your_name = forms.CharField(label='your name', max_length=100)

class HelloForm(forms.Form):
    your_name = forms.CharField(
        label='name',
        max_length=20,
        required=True,
        widget=forms.TextInput()
    )


