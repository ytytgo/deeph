from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('', views.Home.as_view(), name='home'),
    path('tweepy',views.tweepy),
    path('tweepy2',views.tweepy),
    path('favfollow',views.favfollow),

    path('love',views.love),

    path('registration/',include('registration.urls')),

    path('home_timeline',views.home_timeline, name='home_timeline'),
    path('twint',views.twint,name='twint'),
    path('post/new/', views.post_new, name='post_new'),


    path('love2',views.love2),
    path('love3',views.love3),
    path('home2',views.Home2.as_view(), name='home2'),
    path('home3',views.Home3.as_view(), name='home3'),
    path('q',views.Q.as_view(),name='q'),

    # connect to other app urls

    path('search-form',views.search_form),


### twitter get post request
    path('get_query',views.get_query, name="get_query"),
    path('search',views.search,name='search'),
    path('get_request', views.get_request,name='get_request'),
    path('popen',views.popen, name='popen'),
    path('condition',views.condition),
    path('tohtml',views.tohtml,name='tohtml'),
    path('get',views.get, name='get'),
    path('get2',views.get2, name='get2'),
    path('love3',views.love3, name='love3'),    
    path('love4',views.love4, name='love4'),
    path('love5m',views.love5m, name='love5m'),   
    path('f1000s',views.f1000s,name='f1000s'), 

    path('q',views.q, name='q'),
    path('q2',views.q2, name='q2'),

    path('api-auth', include('rest_framework.urls')),



]


