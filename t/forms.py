from allauth.account.forms import SignupForm
from django import forms

class CustomSignupForm(SignupForm):
    screen_name = forms.CharField(max_length=30, label='screen_name')
    search = forms.CharField(maz_length=100, label='search')
    def signup(self,request,user):
        user.screen_name = self.cleaned_data['screen_name']
        user.search = self.cleaned_data['search']
        user.save()
        return user

from django import forms
from .models import GeeksModel
class GeeksForm(forms.ModelForm):
    class Meta:
        model = GeeksModel
        fields = "__all__"

class VehicleForm(forms.Form):
    make = forms.CharField()
    model = forms.CharField()
    year = forms.IntegerField()


from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'text',)


from django import forms  
from myapp.models import Employee  
  
class EmployeeForm(forms.ModelForm):  
    class Meta:  
        model = Employee  
        fields = "__all__"  