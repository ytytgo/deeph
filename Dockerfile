FROM python:3.7
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1
# Set work directory
WORKDIR /code
# Install dependencies
COPY requirements.txt /code/
RUN pip install -r requirements.txt
# Copy project
COPY . /code/

# now no need tf, veru heavy!!
# RUN pip install --no-cache-dir tensorflow

RUN python3 manage.py makemigrations
RUN python3 manage.py migrate

CMD [ "python3", "manage.py", "runserver", "0.0.0.0:80" ]
#CMD [ "gunicorn t.wsgi --bind 0.0.0.0:80"]

